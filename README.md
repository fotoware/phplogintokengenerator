# Login Tokens #

This repository lets you generate login tokens in PHP, which are used to authenticate users in FotoWeb.

### What are login tokens? ###

* [Documentation of login tokens](http://learn.fotoware.com/02_FotoWeb_8.0/Developing_with_the_FotoWeb_API/Login_Tokens)
* Login tokens are also used for [user authentication in embeddable widgets](http://learn.fotoware.com/02_FotoWeb_8.0/Integrating_FotoWeb_with_third-party_systems/User_Authentication_for_Embeddable_Widgets)
* Login tokens are also used for [user authentication in the legacy ArchiveAgent API](http://learn.fotoware.com/02_FotoWeb_8.0/Developing_with_the_FotoWeb_API/The_FotoWeb_Archive_Agent_API/User_authentication)


### How do I use the generator? ###

* Import the LoginTokenGenerator file to your project
* Create a new Generator like this:

```
#!php
// first parameter is the encryption secret from your FotoWeb server
// second paramaeter is a boolean which tells if you are using it with a widget or not.
$tokenGenerator = new LoginTokenGenerator('1234567abcdef', true);
```

* Create a new token like this:

```
#!php
//parameter is the username that you want to log in with
$loginToken = $tokenGenerator->CreateLoginToken('user1');
```

Now you have a token that you can use to authenticate


### How do I integrate this code into my own? ###

* You are free to use this code in your own proprietary products. Please see [LICENSE.txt](https://bitbucket.org/fotoware/phplogintokengenerator/src/2db5b80c8dd923fe9c955e8c1f71d5571b298dc0/LICENSE.txt?at=master&fileviewer=file-view-default) for more details.
* IMPORTANT: Always generate login tokens on the server side! NEVER expose the encryption secret of your server to the public (e.g., in client-side JavaScript)!